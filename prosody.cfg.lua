-- Prosody XMPP Server Configuration
--
-- Information on configuring Prosody can be found on our
-- website at https://prosody.im/doc/configure
--
-- Tip: You can check that the syntax of this file is correct
-- when you have finished by running this command:
--     prosodyctl check config
-- If there are any errors, it will let you know what and where
-- they are, otherwise it will keep quiet.
--
-- Good luck, and happy Jabbering!


---------- Server-wide settings ----------
-- Settings in this section apply to the whole server and are the default settings
-- for any virtual hosts

-- This is a (by default, empty) list of accounts that are admins
-- for the server. Note that you must create the accounts separately
-- (see https://prosody.im/doc/creating_accounts for info)
-- Example: admins = { "user1@example.com", "user2@example.net" }

admins = { "michel@lebihan.pl" }

-- Enable use of libevent for better performance under high load
-- For more information see: https://prosody.im/doc/libevent
--use_libevent = true

-- Prosody will always look in its source directory for modules, but
-- this option allows you to specify additional locations where Prosody
-- will look for modules first. For community modules, see https://modules.prosody.im/

plugin_paths = { "/usr/lib/prosody/michel-modules", "/usr/lib/prosody/modules/mod_mam", "/usr/lib/prosody/prosody-modules" }

-- This is the list of modules Prosody will load on startup.
-- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
-- Documentation for bundled modules can be found at: https://prosody.im/doc/modules

modules_enabled = {

	-- Generally required
		"roster"; -- Allow users to have a roster. Recommended ;)
		"saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
		"tls"; -- Add support for secure TLS on c2s/s2s connections
		"dialback"; -- s2s dialback support
		"disco"; -- Service discovery

	-- Not essential, but recommended
		"carbons"; -- Keep multiple clients in sync
		"pep"; -- Enables users to publish their mood, activity, playing music and more
		"private"; -- Private XML storage (for room bookmarks, etc.)
		"blocklist"; -- Allow users to block communications with other users
		--"vcard"; -- Allow users to set vCards

	-- Nice to have
		"version"; -- Replies to server version requests
		"uptime"; -- Report how long server has been running
		"time"; -- Let others know the time here on this server
		"ping"; -- Replies to XMPP pings with pongs
		"register"; -- Allow users to register on this server using a client and change passwords
		"mam"; -- Store messages in an archive and allow users to access it

	-- Admin interfaces
		"admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
		--"admin_telnet"; -- Opens telnet console interface on localhost port 5582
	
	-- HTTP modules
		--"bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
		--"websocket"; -- XMPP over WebSockets
		"http";
		--"http_files"; -- Serve static files from a directory over HTTP

	-- Other specific functionality
		--"limits"; -- Enable bandwidth limiting for XMPP connections
		--"groups"; -- Shared roster support
		--"server_contact_info"; -- Publish contact information for this service
		--"announce"; -- Send announcement to all online users
		--"welcome"; -- Welcome users who register accounts
		"watchregistrations"; -- Alert admins of registrations
		--"motd"; -- Send a message to users when they log in
		--"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
		"proxy65"; -- Enables a file transfer proxy service which clients behind NAT can use
		"block_registrations";
		"watchuntrusted";
		"e2e_policy";
		--"pep_vcard_avatar";
		"profile";
		--"firewall";
		--"update_client_reminder";
		"lastlog";
		"checkcerts";

	-- Other for Conversations
		"smacks";
		"smacks_offline";
		"csi";
		"csi_battery_saver";

	-- Use cloud_notify for push on iOS and possibly Conversations
		"cloud_notify";

	-- Use http registration. Will be served at https://lebihan.pl/register_web
		"register_web";

	-- Munin plugin
		"munin";
		"measure_client_presence";
		"measure_cpu";
		"measure_malloc";
		"measure_memory";
		"measure_stanza_counts";
};

statistics = "internal"
munin_node_name = "prosody.lebihan.pl"
munin_ports = { 4950 }
munin_interfaces = { "127.0.0.1" }

block_registrations_users = { "admin", "root", "xmpp" }
block_registrations_matching = {
  "master$" -- matches anything ending with master: postmaster, hostmaster, webmaster, etc.
}

e2e_policy_muc = "none";
e2e_policy_whitelist = { "console@lebihan.pl", "prosody@conference.prosody.im", "conversations@conference.siacs.eu", "chat@conference.jabber.at", "zmichoc@conference.lebihan.pl", "michel@lebihan.pl" }
e2e_policy_message_optional_chat = "For security reasons, OMEMO encryption is STRONGLY recommended for conversations on this server."

http_interfaces = { "127.0.0.1" }
https_interfaces = { "127.0.0.1" }

registration_watchers = { "michel@lebihan.pl" }

vcard_to_pep = true;

-- Prevents clients from hogging all of the fds with unauthed c2s.
c2s_timeout = 120;

--firewall_scripts = { "/etc/prosody/ruleset.pfw", "/etc/prosody/ruleset_bad_words.pfw" }

-- These modules are auto-loaded, but should you want
-- to disable them then uncomment them here:
modules_disabled = {
	-- "offline"; -- Store offline messages
	-- "c2s"; -- Handle client connections
	-- "s2s"; -- Handle server-to-server connections
};

-- Disable account creation by default, for security
-- For more information see http://prosody.im/doc/creating_accounts
allow_registration = false;
-- allow_registration = true;

-- Debian:
--   send the server to background.
--
daemonize = true;

-- Debian:
--   Please, don't change this option since /var/run/prosody/
--   is one of the few directories Prosody is allowed to write to
--
pidfile = "/var/run/prosody/prosody.pid";

-- These are the SSL/TLS-related settings. If you don't want
-- to use SSL/TLS, you may comment or remove this
ssl = {
	key = "/etc/prosody/certs/privkey.pem";
	certificate = "/etc/prosody/certs/fullchain.pem";
	dhparam = "certs/dh-4096.pem";
	protocol = "tlsv1_2+";
	ciphers = "HIGH+kEECDH:HIGH+kEDH:!CAMELLIA:!PSK:!SRP:!3DES:!aNULL";
	curve = "X25519:P-384:P-256:P-521";
}

c2s_ports = { 5222 }
legacy_ssl_ports = { 5223 };

-- Force clients to use encrypted connections? This option will
-- prevent clients from authenticating unless they are using encryption.

c2s_require_encryption = true
s2s_require_encryption = true

-- Force certificate authentication for server-to-server connections?
-- This provides ideal security, but requires servers you communicate
-- with to support encryption AND present valid, trusted certificates.
-- NOTE: Your version of LuaSec must support certificate verification!
-- For more information see http://prosody.im/doc/s2s#security

--s2s_secure_auth = false
s2s_secure_auth = true

-- Many servers don't support encryption or have invalid or self-signed
-- certificates. You can list domains here that will not be required to
-- authenticate using certificates. They will be authenticated using DNS.

--s2s_insecure_domains = { "gmail.com" }
s2s_insecure_domains = { "jabber.ced117.net", "conference.jabber.fsfe.org" }

-- Even if you leave s2s_secure_auth disabled, you can still require valid
-- certificates for some domains by specifying a list here.

--s2s_secure_domains = { "jabber.org" }

untrusted_ignore_domains = { "xmpp.net", "check.messaging.one" }

-- Select the authentication backend to use. The 'internal' providers
-- use Prosody's configured data storage to store the authentication data.
-- To allow Prosody to offer secure authentication mechanisms to clients, the
-- default provider stores passwords in plaintext. If you do not trust your
-- server please see http://prosody.im/doc/modules/mod_auth_internal_hashed
-- for information about using the hashed backend.

-- authentication = "internal_plain"
authentication = "internal_hashed"

-- Select the storage backend to use. By default Prosody uses flat files
-- in its configured data directory, but it also supports more backends
-- through modules. An "sql" backend is included by default, but requires
-- additional dependencies. See http://prosody.im/doc/storage for more info.

storage = "internal"
storage = {
    muc_log = "xmlarchive";
}

--storage = "sql" -- Default is "internal" (Debian: "sql" requires one of the
-- lua-dbi-sqlite3, lua-dbi-mysql or lua-dbi-postgresql packages to work)

-- For the "sql" backend, you can uncomment *one* of the below to configure:
--sql = { driver = "SQLite3", database = "prosody.sqlite" } -- Default. 'database' is the filename.
--sql = { driver = "MySQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }
--sql = { driver = "PostgreSQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }

-- Logging configuration
-- For advanced logging see http://prosody.im/doc/logging
--
-- Debian:
--  Logs info and higher to /var/log
--  Logs errors to syslog also
log = {
	-- Log files (change 'info' to 'debug' for debug logs):
	debug = "/var/log/prosody/prosody.log";
	error = "/var/log/prosody/prosody.err";
	-- Syslog:
	{ levels = { "error" }; to = "syslog";  };
}

----------- Virtual hosts -----------
-- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
-- Settings under each VirtualHost entry apply *only* to that host.

VirtualHost "lebihan.pl"
	-- enabled = false -- Remove this line to enable this host

	-- Assign this host a certificate for TLS, otherwise it would use the one
	-- set in the global section (if any).
	-- Note that old-style SSL on port 5223 only supports one certificate, and will always
	-- use the global one.
	--ssl = {
	--	key = "/etc/prosody/certs/lebihan.key";
	--	certificate = "/etc/prosody/certs/lebihan.crt";
	--}
	modules_enabled = {
		"http_upload_external";
	}
http_host = "localhost"

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see http://prosody.im/doc/components

http_upload_external_file_size_limit = 10*1024*1024*1024
http_upload_external_secret = "password"
http_upload_external_base_url = "URL"

---Set up a MUC (multi-user chat) room server on conference.example.com:
--Component "conference.example.com" "muc"
Component "conference.lebihan.pl" "muc"
modules_enabled = {
  "muc_mam",
  "vcard_muc",
}

muc_log_by_default = true; -- Enable logging by default (can be disabled in room config)
--muc_log_all_rooms = true; -- set to true to force logging of all rooms

muc_log_presences = false;

---Set up an external component (default component port is 5347)
--
-- External components allow adding various services, such as gateways/
-- transports to other networks like ICQ, MSN and Yahoo. For more info
-- see: http://prosody.im/doc/components#adding_an_external_component
--
--Component "gateway.example.com"
--	component_secret = "password"

--Component "biboumi.lebihan.pl"
--	component_secret = "password"

Component "console@lebihan.pl" "admin_message"

------ Additional config files ------
-- For organizational purposes you may prefer to add VirtualHost and
-- Component definitions in their own config files. This line includes
-- all config files in /etc/prosody/conf.d/

Include "conf.d/*.cfg.lua"
